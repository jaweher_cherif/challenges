function ArrayChallenge  (arr) {
    var nodes = new Set();
    arr = arr.map(elem => {
      var arc = {
        to: elem.split(',')[0].split('(')[1],
        from: elem.split(',')[1].split(')')[0],
      };
      nodes.add(arc.from);
      nodes.add(arc.to);
      return arc;
    });
    var occurencesTo = new Map();
    var occurencesFrom = new Map();
    for (var node of nodes) {
      occurencesTo.set(node, 0);
      occurencesFrom.set(node, 0);
    }

    //verify max incoming nodes = 1 && max outgoing = 2
    for (var arc of arr) {
      var occTo = occurencesTo.get(arc.to);
      var occFrom = occurencesFrom.get(arc.from);
      if (occFrom >= 2 || occTo >= 1) return false;
      occurencesTo.set(arc.to, occTo + 1);
      occurencesFrom.set(arc.from, occFrom + 1);
    }
  
    //verify only 1 start node
    var numberStartNodes = 0;
    var startNode;
    for (var node of nodes) {
      if (occurencesTo.get(node) === 0) {
        numberStartNodes++;
        startNode = node;
      }
    }
    if (numberStartNodes !== 1) return false;
  
    //verify no loops (tree)
    var visited = new Map();
    for (var node of nodes) {
      visited.set(node, false)
    }
    var nodesToVisit = [startNode];
    while(nodesToVisit.length>0){
      var node = nodesToVisit[0];
      if(visited.get(node)) return false;
      visited.set(node,true);
      for (var arc of arr) {
        if (arc.from === node) nodesToVisit.push(arc.to)
      }
      nodesToVisit.shift()
    }
    return true
  };

  console.log(ArrayChallenge(["(1,2)","(3,2)","(2,12)","(5,2)"]));
