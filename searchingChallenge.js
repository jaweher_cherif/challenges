function SearchingChallenge(str) { 
    var k = parseInt(str.charAt(0));
    var i=1;
    var j=1;
    var listSubstr=[];
    while(i<str.length && j<str.length){
        var char= str.charAt(i);
        var characters=new Set(char);
        var substr=char;
        j=i+1;
        p=i+1;
        while(j<str.length){
            if(!characters.has(str.charAt(j))){
                if(characters.size==k)break;
                characters.add(str.charAt(j));
            }
            substr=substr+str.charAt(j);
            j++;
            if(str.charAt(p)==char){
                i=p-1;
            }
            p++;
        }
        i++;
        listSubstr.push(substr);
       
    } 
    var maxStr="";
    var maxStrLen=0;
    for(i=0; i<listSubstr.length;i++ ){
        if(listSubstr[i].length > maxStrLen){
            maxStr=listSubstr[i];
            maxStrLen=listSubstr[i].length;
        }
    }
    return maxStr; 
  }   
  // keep this function call here 
  console.log(SearchingChallenge("3aabacbebebe"));
